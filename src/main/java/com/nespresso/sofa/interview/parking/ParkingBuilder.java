package com.nespresso.sofa.interview.parking;

/**
 * Builder class to get a parking instance
 */
public class ParkingBuilder {

    ParkingImpl parking = new ParkingImpl();

    public ParkingBuilder withSquareSize(final int size) {
        parking.setSquareSize(size);
        return this;
    }

    public ParkingBuilder withPedestrianExit(final int pedestrianExitIndex) {
        parking.addPedestrianExit(pedestrianExitIndex);
        return this;
    }

    public ParkingBuilder withDisabledBay(final int disabledBayIndex) {
        parking.addDisabledBay(disabledBayIndex);
        return this;
    }

    public Parking build() {
        return parking;
    }
}
