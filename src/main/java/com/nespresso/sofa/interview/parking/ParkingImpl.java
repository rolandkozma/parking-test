package com.nespresso.sofa.interview.parking;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ParkingImpl implements Parking {

    private int squareSize;
    private final List<Integer> pedestrianExits = new ArrayList<>();
    private final List<Integer> disabledBays = new ArrayList<>();
    private final Map<Integer, Character> occupiedBays = new HashMap<>();

    public ParkingImpl() {
    }

    public int getSquareSize() {
        return squareSize;
    }

    public void setSquareSize(int squareSize) {
        this.squareSize = squareSize;
    }

    public List<Integer> getPedestrianExits() {
        return pedestrianExits;
    }

    public List<Integer> getDisabledBays() {
        return disabledBays;
    }

    public void addPedestrianExit(int pedestrianExitIndex) {
        pedestrianExits.add(pedestrianExitIndex);
        pedestrianExits.sort(Comparator.comparingInt(a -> a));
    }

    public void addDisabledBay(int disabledBayIndex) {
        disabledBays.add(disabledBayIndex);
        disabledBays.sort(Comparator.comparingInt(a -> a));
    }

    @Override
    public int getAvailableBays() {
        return squareSize * squareSize - pedestrianExits.size() - occupiedBays.size();
    }

    @Override
    public int parkCar(char carType) {
        return (!isDisabledCar(carType)) ? parkRegularCar(carType) : parkDisabledCar(carType);
    }

    @Override
    public boolean unparkCar(int index) {
        boolean removed = false;
        if (!isFreeBay(index)) {
            occupiedBays.remove(index);
            removed = true;
        }
        return removed;
    }

    @Override
    public String toString() {
        return IntStream.range(0, squareSize)
                .mapToObj(this::getStringRepresentationOfARow)
                .collect(Collectors.joining("\n"));
    }

    private int parkRegularCar(char carType) {
        Map<Integer, Integer> exitToFoundBayMap = pedestrianExits.stream()
                .collect(Collectors.toMap(Function.identity(), this::findBayIndexForRegularCar));

        Integer bayIndex = exitToFoundBayMap.entrySet().stream()
                .min(Comparator.comparingInt(entry -> distanceFromPedestrianExit(entry.getKey(), entry.getValue())))
                .orElse(new AbstractMap.SimpleEntry<>(-1, -1))
                .getValue();

        if (bayIndex != -1) {
            occupiedBays.put(bayIndex, carType);
        }

        return bayIndex;
    }

    private int findBayIndexForRegularCar(int pedestrianExitIndex) {
        int distance = 1;
        boolean foundFreeBay = false;
        int bayIndex;
        do {
            bayIndex = chooseBayIndexForRegularCar(pedestrianExitIndex, distance);
            if (bayIndex != -1) {
                foundFreeBay = true;
            } else {
                distance++;
            }
        }
        while (!foundFreeBay || distance >= (squareSize * squareSize) - 1);
        return bayIndex;
    }

    private int chooseBayIndexForRegularCar(int pedestrianExitIndex, int distance) {
        int result = -1;

        int leftBayIndex = pedestrianExitIndex - distance;
        if (isFreeBay(leftBayIndex) && !isDisabledBay(leftBayIndex) && !isPedestrianExitBay(leftBayIndex)) {
            result = leftBayIndex;
        } else {
            int rightBayIndex = pedestrianExitIndex + distance;
            if (isFreeBay(rightBayIndex) && !isDisabledBay(rightBayIndex) && !isPedestrianExitBay(rightBayIndex)) {
                result = rightBayIndex;
            }
        }
        return result;
    }

    private int parkDisabledCar(char carType) {
        Map<Integer, Integer> distanceFromExitMap = disabledBays.stream()
                .filter(this::isFreeBay)
                .collect(Collectors.toMap(Function.identity(), this::distanceFromPedestrianExit));

        int bayIndex = distanceFromExitMap.entrySet().stream()
                .min(Comparator.comparingInt(Map.Entry::getValue))
                .orElse(new AbstractMap.SimpleEntry<>(-1, -1)).getKey();

        if (bayIndex != -1) {
            occupiedBays.put(bayIndex, carType);
        }

        return bayIndex;
    }

    private int distanceFromPedestrianExit(int bayIndex) {
        return pedestrianExits.stream()
                .map(pedestrianExitIndex -> distanceFromPedestrianExit(pedestrianExitIndex, bayIndex))
                .min(Comparator.comparingInt(a -> a))
                .orElse(-1);
    }

    private String getStringRepresentationOfARow(int i) {
        List<String> elements = IntStream.range(0, squareSize)
                .mapToObj(element -> getStringRepresentation((i * squareSize) + element))
                .collect(Collectors.toList());
        if (i % 2 != 0) {
            Collections.reverse(elements);
        }
        return elements.stream().collect(Collectors.joining());
    }

    private String getStringRepresentation(int i) {
        if (pedestrianExits.contains(i)) {
            return "=";
        } else if (isDisabledBay(i) && isFreeBay(i)) {
            return "@";
        } else if (!isDisabledBay(i) && isFreeBay(i)) {
            return "U";
        } else if (isDisabledBay(i) && !isFreeBay(i)) {
            return "D";
        } else {
            return occupiedBays.get(i).toString();
        }
    }

    private boolean isFreeBay(int bayIndex) {
        return !occupiedBays.keySet().contains(bayIndex);
    }

    private boolean isDisabledBay(int bayIndex) {
        return disabledBays.contains(bayIndex);
    }

    private boolean isPedestrianExitBay(int bayIndex) {
        return pedestrianExits.contains(bayIndex);
    }

    private static int distanceFromPedestrianExit(int pedestrianExitIndex, int bayIndex) {
        return Math.abs(pedestrianExitIndex - bayIndex);
    }

    private static boolean isDisabledCar(char carType) {
        return carType == 'D';
    }
}
